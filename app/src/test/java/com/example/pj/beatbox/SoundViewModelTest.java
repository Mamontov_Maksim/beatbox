package com.example.pj.beatbox;

import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Test;


import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class SoundViewModelTest {
    private BeatBox mBeatBox;
    private Sound mSound;
    private SoundViewModel mSubject;


    @Before
    public void setUp() throws Exception {
        /**
         * mock - статический метод библеотеки Mockito для создания фиктивного обьекта
         */
        mBeatBox = mock(BeatBox.class);
        mSound = new Sound("assetPath");
        mSubject = new SoundViewModel(mBeatBox);
        mSubject.setSound(mSound);
    }

    @Test
    public void exposesSoundNameAdTitle(){
        MatcherAssert.assertThat(mSubject.getTitle(), is(mSound.getName()));
    }

    @Test
    public void callsBeatBoxPlayOnButtonClicked(){
        mSubject.onButtonClicked();
        /**
         *  verify - проверяет вызвал ли метод нужный обьект
         */
        verify(mBeatBox).play(mSound);
    }
}