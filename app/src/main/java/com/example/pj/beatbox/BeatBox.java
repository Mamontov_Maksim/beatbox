package com.example.pj.beatbox;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BeatBox {

    private static final String TAG = "BeatBox";
    private static final String SOUNDS_FOLDER = "sample_sound";
    private static final int MAX_SOUNDS = 5;

    private AssetManager mAsset;
    private List<Sound> mSounds = new ArrayList<>();
    private SoundPool mSoundPool;
    private float mSpeedSound = 1.0f;



    public BeatBox(Context context){
        mAsset = context.getAssets();
        mSoundPool = new SoundPool(MAX_SOUNDS, AudioManager.STREAM_MUSIC, 0);
        loadSounds();
    }

    private void loadSounds(){
        String[] soundsName;
        try{
            soundsName = mAsset.list(SOUNDS_FOLDER);
            Log.d(TAG, "found " + soundsName.length + " sounds");
        }catch (IOException e){
            Log.e(TAG, "Could not list assets", e);
            return;
        }

        for (String fileName : soundsName){
            try {
                String assetPath = SOUNDS_FOLDER + "/" + fileName;
                Sound sound = new Sound(assetPath);
                load(sound);
                mSounds.add(sound);
            } catch (IOException e){
                Log.e(TAG, "Not load sounds", e);
            }
        }
    }

    private void load(Sound sound) throws IOException {
        AssetFileDescriptor afd = mAsset.openFd(sound.getAssetPath());
        int soundId = mSoundPool.load(afd, 1);
        sound.setSoundId(soundId);
    }

    public void play(Sound sound){
        Integer soundId = sound.getSoundId();
        if (soundId == null){
            return;
        }
        mSoundPool.play(soundId, 1.0f, 1.0f, 1, 0, mSpeedSound);
    }

    public void release(){
        mSoundPool.release();
    }

    public List<Sound> getSounds(){
        return mSounds;
    }

    public void setSpeedSound(float mSpeedSound) {
        this.mSpeedSound = mSpeedSound;
    }
}
